﻿$(document).ready(function () {

    $('.soloNumeros').on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $("#btnActualizar").click(function (e) {

        function isEmail() {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test($('#Email').val());
        }


        let error = "";

        if (!$("#Primer_nombre").val() || $("#Primer_nombre").val().trim().length < 1) {
            error += "Ingrese primer nombre.\n";
        }

        if ($('#Primer_nombre').val().length > 50) {

            error += "El nombre no puede tener más de 50 caracteres.\n";
        }

        if (!$('#Segundo_nombre').val() || $("#Segundo_nombre").val().trim().length < 1) {
            error += "Ingrese segundo nombre.\n";
        }

        if ($('#Segundo_nombre').val().length > 50) {
            error += "El segundo nombre no puede tener más de 50 caracteres.\n";
        }

        if (!$('#Primer_apellido').val() || $("#Primer_apellido").val().trim().length < 1) {
            error += "Ingrese primer apellido.\n";
        }

        if ($('#Primer_apellido').val().length > 50) {
            error += "El primer apellido no puede tener más de 50 caracteres.\n";
        }

        if (!$('#Segundo_apellido').val() || $("#Segundo_apellido").val().trim().length < 1) {
            error += "Ingrese segundo apellido.\n";
        }

        if ($('#Segundo_apellido').val().length > 50) {
            error += "El segundo apellido no puede tener más de 50 caracteres.\n";
        }

        if (!$('#Numero_telefonico').val() || $("#Numero_telefonico").val().trim().length < 1) {
            error += "Ingrese número telefónico.\n";
        }

        if (!isEmail()) {
            error += "Email no válido.\n";
        }
        if ($('#Email').val().length > 50) {
            error += "El email no puede tener más de 50 caracteres.\n";
        }

        if (!$('#Contrasena').val()) {
            error += "Ingrese una contraseña.\n"
        }

        if ($('#Contrasena').val().length > 0 && $('#Contrasena').val().length < 5) {
            error += "La contrasena debe tener al menos 5 caracteres.\n";
        }

        if ($('#Contrasena').val().length > 50) {
            error += "La contraseña no puede tener más de 50 caracteres.\n";
        }

        if ($('#Confirma_Contrasena').val() != $('#Contrasena').val()) {
            error += "Las contraseñas no coinciden.\n";
        }

        if (error != "") {
            e.preventDefault();
            swal("Datos incompletos", error, "error");
        }

        if (error == "") {           
            e.preventDefault();
            swal({
                title: "Administrador actualizado",
                text: "Vuelva a logearse para ver reflejados los cambios =)",
                icon: "success"
            }).then(function () {
                $('#adminEditForm').submit();
            });
        }
    });
});