﻿function enviaValidaCategoriaV3(selector) {

    $(selector).click(function (e) {

        let error = "";
        let hayDesactivada = "";

        if (!$('#Codigo_categoria').val().trim() || $("#Codigo_categoria").val().trim().length < 1) {
            error += "Ingrese código para la categoría.\n";
        }


        if ($('#Codigo_categoria').val().length > 255) {
            error += "El código de la categoría no puede tener más de 255 caracteres.\n";
        }

        //if ($('#Id_estado').val() == '') {
        //    error += "No ha seleccionado ningún estado inical para la categoría.\n";
        //}

        if (!$('#Nombre_categoria').val().trim() ) {

            error += "Ingrese un nombre a la categoría.\n"
        }

        if ($('#Nombre_categoria').val().length > 25) {
            error += "El nombre de la categoría no puede tener más de 25 caracteres.\n";
        }

        if (!$('#Descripcion_categoria').val().trim() || $("#Descripcion_categoria").val().trim().length < 1) {

            error += "Ingrese una descripción para la categoría.\n"
        }

        if ($('#Descripcion_categoria').val().length > 255) {
            error += "La Descripción de la categoría no puede tener más de 255 caracteres.\n";
        }

       
         //if ($('.desactivada').prop('checked')) {
         //    hayDesactivada = "*";
         //}

        if ($('input:checkbox.desactivada:checked').length > 0) {
            hayDesactivada = "*";
        }
        
        if (error != "") {
            e.preventDefault();
            swal("Verifique los datos", error, "error");
        }

        if (error == "" && hayDesactivada=="") {
            e.preventDefault()
            swal({
                title: "Categoría Actualizada",
                icon: "success"
            }).then(function () {
                asociaCategoriaMarcaEdit()
                window.location.replace("/Categorias/ListarCategoria/");
            });
        }

        if (error == "" && hayDesactivada == "*") {
            e.preventDefault()
            swal({
                title: "Categoría Actualizada",
                text: "Una o más marcas seleccionadas se encuentran deshabilitadas, puede activarlas en el panel de marcas.",
                icon: "warning"
            }).then(function () {
                asociaCategoriaMarcaEdit()
                window.location.replace("/Categorias/ListarCategoria/");
            });
        }

    });
}

function asociaCategoriaMarcaEdit() {
    var resultadoAsociarCategoriaMarca = JSON.stringify({ Id_categoria: $('#Id_categoria').val(),idsDesactivar:idsDesactivar, ids: ids, Codigo_categoria: $('#Codigo_categoria').val().trim(), Id_estado: $('#Id_estado').val(), Nombre_categoria: $('#Nombre_categoria').val().trim(), Descripcion_categoria: $('#Descripcion_categoria').val().trim() });
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        url: '/Categorias/EditarCategoriaAsociarCategoriaMarca/',
        data: resultadoAsociarCategoriaMarca,
        success: function (result) {
            if (result == "") {
                //en caso de que se necesite...
            }
            
        }

    });
}