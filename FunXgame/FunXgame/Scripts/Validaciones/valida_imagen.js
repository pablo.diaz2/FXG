﻿function enviaValidaImagen() {

    $('#btnAgregarImg').click(function (e) {

        let error = "";

        if (!$('#Nombre_imagen').val() || $("#Nombre_imagen").val().trim().length < 1) {
            error += "Ingrese nombre de imagen.\n";
        }

        if ($('#Nombre_imagen').val().length > 20) {
            error += "El nombre de la imagen no puede exceder los 20 caracteres.\n";
        }

        if (!$('#Ruta').val()) {
            error += "No ha seleccionado ningún archivo de imagen.\n";
        }

        if (!$('#Grupo_imagen').val()) {

            error += "Seleccione grupo al que pertenece la imagen.\n"
        }

        if (error != "") {
            e.preventDefault();
            swal("Verifique los datos", error, "error");
        }

        if (error == "") {
            e.preventDefault()
            swal({
                title: "Imagen agregrada a la librería",
                icon: "success"
            }).then(function () {
                $('#imagenCrearForm').submit();
                
            });
            
        }
    });
}



function enviaValidaImagenEdicion(selector, msgfinal) {

    $(selector).click(function (e) {

        let error = "";

        if (!$('#Nombre_imagen').val() || $("#Nombre_imagen").val().trim().length < 1) {
            error += "Ingrese nombre de imagen.\n";
        }

        if ($('#Nombre_imagen').val().length > 20) {
            error += "El nombre de la imagen no puede exceder los 20 caracteres.\n";
        }

        if (!$('#Grupo_imagen').val()) {

            error += "Seleccione grupo al que pertenece la imagen.\n"
        }

        if (error != "") {
            e.preventDefault();
            swal("Verifique los datos", error, "error");
        }

        if (error == "") {
            e.preventDefault()
            swal({
                title: msgfinal,
                icon: "success"
            }).then(function () {
                $('#imagenEditarForm').submit();
            });
        }
    });
}

