﻿

function enviaValidaProducto(msg,txt,idForm) {

    $('.soloNumeros').on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('#btnGuardar').click(function (e) {
        
        let error = '';

        if (!$('#Codigo_producto').val().trim()) {
            error += "Ingrese código para el producto.\n";
        }

        if ($('#Codigo_producto').val().length > 255) {
            error += "El código del producto no puede tener más de 255 caracteres.\n";
        }

        if ($('#Id_estado').val() == '') {
            error+="Ingrese un estado inicial para el producto.\n"
        }

        if ($('#Id_marca').val() == '') {
            error+="Debe seleccionar una marca para el producto.\n"
        }

        if ($('#select_categoria').val() == '') {
            error+="Debe seleccionar una categoría para el producto.\n"
        }

        if ($('#img_select').val() == '') {
            error+="Debe seleccionar una imagen para el producto.\n"
        }

        if (!$('#Nombre_producto').val().trim()) {
            error += "Ingrese un nombre para el producto.\n";
        }

        if ($('#Nombre_producto').val().length > 50) {
            error += "El nombre del producto no puede tener más de 50 caracteres.\n";
        }

        
        if (!$('#Precio_producto').val().trim()) {
            error += "Ingrese un precio para el producto.\n";
        }

        
        if (!$('#Stock_producto').val()) {
            error += 'Ingrese un stock para el producto.\n';
        }

        if ($('#Stock_producto').val() < 0) {
            error += "No puede ingresar un stock negativo.\n";
        }

        if (!$('#Descripcion_producto').val().trim()) {
            error += "Debe ingresar una descripción para el producto.\n";
        }

        if ($('#Descripcion_producto').val().length > 255) {
            error+="La descripción del producto no puede tenr más de 255 caracteres.\n"
        }

        if (error != '') {
            e.preventDefault();
            swal('Verifique los datos ingresados', error, "error");
        }

        if (error == '') {
            e.preventDefault()
            swal({
                title: msg,
                text: txt,
                icon: "success"
            }).then(function () {
                $(idForm).submit();
            });
        }
    });
}