﻿function enviaValidaCategoriaV2(selector) {

    $(selector).click(function (e) {

        let error = "";
        let hayDesactivada = "";

        if (!$('#Codigo_categoria').val().trim() || $("#Codigo_categoria").val().trim().length < 1) {
            error += "Ingrese código para la categoría.\n";
        }


        if ($('#Codigo_categoria').val().length > 255) {
            error += "El código de la categoría no puede tener más de 255 caracteres.\n";
        }

        if ($('#Id_estado').val() == '') {
            error += "No ha seleccionado ningún estado inical para la categoría.\n";
        }

        if (!$('#Nombre_categoria').val().trim()) {

            error += "Ingrese un nombre a la categoría.\n"
        }

        if ($('#Nombre_categoria').val().length > 25) {
            error += "El nombre de la categoría no puede tener más de 25 caracteres.\n";
        }

        if (!$('#Descripcion_categoria').val().trim() || $("#Descripcion_categoria").val().trim().length < 1) {

            error += "Ingrese una descripción para la categoría.\n"
        }

        if ($('#Descripcion_categoria').val().length > 255) {
            error += "La descripción de la categoría no puede tenr más de 255 caracteres.\n";
        }

        //if ($('.desactivada').prop('checked')) {
        //    hayDesactivada = "*";
        //}

        if ($('input:checkbox.desactivada:checked').length > 0) {
            hayDesactivada = "*";
        }

        if (error != "") {
            e.preventDefault();
            swal("Verifique los datos", error, "error");
        }

        if (error == "" && hayDesactivada=="") {
            e.preventDefault()
            swal({
                title: "Categoría agregrada con éxito",
                icon: "success"
            }).then(function () {
                asociaCategoriaMarca();
            }).then(function () {
                limpiar();
            });
            
        }

        if (error == "" && hayDesactivada == "*") {
            e.preventDefault()
            swal({
                title: "Categoría agregrada con éxito",
                text: "Una o más marcas seleccionadas se encuentran deshabilitadas, puede activarlas en el panel de marcas.",
                icon: "warning"
            }).then(function () {
                asociaCategoriaMarca();
            }).then(function () {
                limpiar();
            });

        }

    });
}

function asociaCategoriaMarca() {
    var resultadoAsociarCategoriaMarca = JSON.stringify({ ids: ids, Codigo_categoria: $('#Codigo_categoria').val().trim(), Id_estado: $('#Id_estado').val(), Nombre_categoria: $('#Nombre_categoria').val().trim(), Descripcion_categoria: $('#Descripcion_categoria').val().trim() });
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        url:'/Categorias/CrearCategoriaAsociarCategoriaMarca/',
        data: resultadoAsociarCategoriaMarca,
        success: function (result) {
            if (result == "noCategoria") {
                swal({
                    title: "No se ha asociado ninguna marca",
                    text: "Puede hacerlo después en el panel de edición de categorías",
                    icon: "warning"
                }).then(function () {
                    
                    //en caso de que se necesite algo
                });
            }
            if (result == "siCategoría") {
                //en caso de que se necesite algo
            }
        }
    });
}

function limpiar() {
        $('#Codigo_categoria').val('');
        $('#Nombre_categoria').val('');
        $('#Descripcion_categoria').val('');
        $('#Id_estado').val('');
        // $(".checkBoxClase").prop('checked', false);   //esta línea hace lo mismo que la línea de abajo pero tomando la clase del elemento, a diferencia del de abajo que lo hace por la etiqueta
        $("input[type=checkbox]").prop('checked', false);
    }