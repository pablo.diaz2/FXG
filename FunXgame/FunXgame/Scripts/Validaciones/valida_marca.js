﻿

function enviaValidaMarca(msg, idform) {

    $('#btnGuardar').click(function (e) {

        let error = "";

        if (!$('#Codigo_marca').val().trim() || $("#Codigo_marca").val().trim().length < 1) {
            error += "Ingrese código para la marca.\n";
        }

        if ($('#Codigo_marca').val().length > 255) {
            error += "El código no puede tener más de 255 caracteres.\n";
        }

        if ($('#Id_estado').val()=='') {
            error += "Ingrese un estado inical para la marca.\n";
        }

        if ($('#img_select').val()=='') {
            error += "Debe seleccionar una imagen para la marca.\n";
        }

        if (!$('#Nombre_marca').val().trim() || $("#Nombre_marca").val().trim().length < 1) {
            error += "Debe seleccionar un nombre para la marca.\n";
        }

        if ($('#Nombre_marca').val().length > 25) {
            error += "El nombre de la marca no puede tener más de 25 caracteres.\n";
        }

        if (!$('#Descripcion_marca').val().trim() || $("#Descripcion_marca").val().trim().length < 1) {
            error += "Debe ingresar una descripcion para la marca.\n";
        }

        if ($('#Descripcion_marca').val().length > 255) {
            error += "La descripción de la marca no puede tener mas de 255 caracteres.\n";
        }

        



        if (error != "") {
            e.preventDefault();
            swal("Verifique los datos ingresados", error, "error");
        }

        if (error == "") {
            e.preventDefault()
            swal({
                title: msg,
                icon: "success"
            }).then(function () {
                $(idform).submit();
                
            });
            
        }

    });
}



