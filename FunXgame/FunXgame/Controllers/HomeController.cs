﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;
using PagedList;
using PagedList.Mvc;


namespace FunXgame.Controllers
{
   
    public class HomeController : Controller 
    {
        db_FunXGameEntities db = new db_FunXGameEntities();

        public ActionResult Index(int? i)
        {
            ViewBag.Marca = GetMarca();
            ViewBag.Marca_Categoria = GetMarca_Categorias();

            var lista = db.Producto.OrderBy(d => d.Marca.Nombre_marca).Where
                (d => d.Id_estado == 1 && d.Categoria.Id_estado == 1 && d.Marca.Id_estado == 1).ToList().ToPagedList(i ?? 1, 6);
            return View(lista);

        }

        public IEnumerable<Marca> GetMarca()
        {
            var lista = db.Marca.OrderBy(d=>d.Nombre_marca).Where(d => d.Id_estado == 1).ToList();
            return (lista);
        }

        //16-12-2021

        public ActionResult ProductoPorCategoria(int? id, int? i)
        {
            if (id != null)
            {
                ViewBag.MarcaCat = GetMarcaCat(); //verificar
                var lista = db.Producto.OrderBy(d => d.Nombre_producto).Where(d => d.Id_categoria == id && d.Id_estado == 1 && d.Marca.Id_estado==1 && d.Categoria.Id_estado==1).ToList().ToPagedList(i ?? 1, 6);
                return View(lista);
            }
            
            return Redirect(Url.Content("~/Home/Index/"));
        }

        public List<Marca_Categoria> GetMarcaCat()
        {
            var lista = db.Marca_Categoria.ToList();
            return lista;
        }

        public ActionResult ProductosPorCategoriaMarca(int? id, int? i)
        {
            var lista = db.Marca_Categoria.Where(d => d.Id_marca_categoria == id && d.Id_estado == 1).ToList();

            if (lista.Count == 0)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }

            if (id != null)
            {
                var marcaCat = db.Marca_Categoria.Where(d => d.Id_marca_categoria == id && d.Id_estado == 1).FirstOrDefault();
                var listaProd = db.Producto.Where(d => d.Id_estado == 1 && d.Id_categoria == marcaCat.Id_categoria && d.Id_marca == marcaCat.Id_marca).ToList().ToPagedList(i ?? 1, 6);
                //int idmar = 0;
                
                ViewBag.MarcaCat = GetMarcaCat(marcaCat.Id_marca_categoria);
                return View(listaProd);
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        public Marca_Categoria GetMarcaCat(int idMarcaCat)
        {
            var marcaCat = db.Marca_Categoria.Find(idMarcaCat);
            return marcaCat;
        }

        public IEnumerable<Marca_Categoria> GetMarca_Categorias()
        {
            var lista = db.Marca_Categoria.Where(d => d.Id_estado == 1);
            return lista;
        }
    }
}