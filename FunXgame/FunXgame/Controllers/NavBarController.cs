﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;
using PagedList;
using PagedList.Mvc;

namespace FunXgame.Controllers
{
    public class NavBarController : Controller
    {
        db_FunXGameEntities db = new db_FunXGameEntities();
        // GET: NabBar
        public ActionResult Nav()
        {
            
            ViewBag.Marca = GetMarca();
            ViewBag.MarcaNombre = GetMarcaNombre();
            ViewBag.Producto = GetProducto();
            ViewBag.Marca_Categoria = GetMarca_Categorias();

            //ultima edición
            ViewBag.Categoria = GetCategoria();
            ViewBag.CategoriaNombre = GetNombreCategoria();
            return PartialView("_Nav");
        }

        // previene que se listen categorias globales -> necesita arreglos
        public IEnumerable<string> GetNombreCategoria()
        {

            var lista = db.Marca_Categoria.Where(d => d.Id_estado == 1 && d.Categoria.Id_estado == 1).ToList();
            
            var listaProducto = db.Producto.Where(d => d.Id_estado == 1 && d.Categoria.Id_estado == 1 && d.Marca.Id_estado == 1);
            var listaFiltrada = new List<string>();
            foreach (var item in listaProducto)
            {
                
                foreach (var item2 in lista)
                {
                    if(item.Categoria.Nombre_categoria == item2.Categoria.Nombre_categoria)
                    {
                        listaFiltrada.Add(item.Categoria.Nombre_categoria);
                    }
                }
            }
            listaFiltrada = listaFiltrada.Distinct().ToList();
            return listaFiltrada;
        }

        public IEnumerable<Categoria> GetCategoria()
        {
            var lista = db.Categoria.Where(d => d.Id_estado == 1).ToList();
            return lista;
        }


        public IEnumerable<Marca> GetMarca()
        {
            var lista = db.Marca.Where(d => d.Id_estado == 1).ToList();
            return lista;
        }

        public IEnumerable<string> GetMarcaNombre()
        {
            //var lista = db.Marca_Categoria.ToList();

            var lista = db.Marca_Categoria.Where(d => d.Categoria.Id_estado == 1 && d.Id_estado==1 );
            var listamarca = db.Marca.Where(d => d.Id_estado == 1);
            var listaFiltrada = new List<string>();

            
            foreach (var item in listamarca)
            {
                foreach (var item2 in lista)
                {
                    if (item.Nombre_marca == item2.Marca.Nombre_marca)
                    {
                        listaFiltrada.Add(item.Nombre_marca);
                    }
                }
            }
            listaFiltrada = listaFiltrada.Distinct().ToList();
            return listaFiltrada;
        }

        public IEnumerable<Marca_Categoria> GetMarca_Categorias()
        {
            var lista = db.Marca_Categoria.Where(d => d.Id_estado == 1);
            return lista;
        }

        

        public IEnumerable<Producto> GetProducto()
        {
            var lista = db.Producto.Where(d => d.Id_estado == 1 && d.Marca.Id_estado==1 && d.Categoria.Id_estado==1).ToList();
            
            return lista;
        }  
    }
}