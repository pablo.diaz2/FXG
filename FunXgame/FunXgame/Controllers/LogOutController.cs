﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FunXgame.Controllers
{
    public class LogOutController : Controller
    {
        // GET: LogOut
        public ActionResult LogOut()
        {
            Session.Abandon();
            return Redirect(Url.Content("~/Home/Index"));
        }
    }
}