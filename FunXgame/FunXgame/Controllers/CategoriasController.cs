﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;
using System.Collections;
using System.Data.Entity;

namespace FunXgame.Controllers
{
    public class CategoriasController : Controller
    {
        db_FunXGameEntities db = new db_FunXGameEntities();
        // GET: Categorias
        public ActionResult ListarCategoria()
        {
            //ViewBag.Id_marca = new SelectList(db.Marca, "Id_marca", "Nombre_marca");
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            return View();
        }

        public ActionResult ListarCategoriaPartial(string codigo, string nombre, int? estado)
        {
            ViewBag.Marcas = GetMarca();
            ViewBag.Marca_Categoria = GetMarcaCategoria();
            if (codigo == "")
            {
                codigo = null;
            }
            if (nombre == "")
            {
                nombre = null;
            }
            var lista = db.Categoria.ToList();
            if (codigo != null)
            {
                lista = lista.Where(d => d.Codigo_categoria.ToLower() == codigo.ToLower()).ToList();
            }
            if (nombre != null)
            {
                lista = lista.Where(d => d.Nombre_categoria.ToLower() == nombre.ToLower()).ToList();
            }
            if (estado != null)
            {
                lista = lista.Where(d => d.Id_estado == estado).ToList();
            }
            return PartialView("_ListarCategoriaPartial", lista);
        }


        //-----CREAR CATEGORIA

        public ActionResult CrearCategoria()
        {
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            ViewBag.Marcas = GetMarca();
            return View();
        }
        
        
        [HttpPost]
        public ActionResult CrearCategoriaAsociarCategoriaMarca(string ids,string Codigo_categoria,string Nombre_categoria,int Id_estado,string Descripcion_categoria)
        {
            
            Categoria categoria = new Categoria();
            categoria.Codigo_categoria = Codigo_categoria;
            categoria.Nombre_categoria = Nombre_categoria;
            categoria.Id_estado = Id_estado;
            categoria.Descripcion_categoria = Descripcion_categoria;
            db.Categoria.Add(categoria);
            db.SaveChanges();
            

            string []idsArr= ids.Split('%');
            List<string> idsList = idsArr.ToList();
            
            foreach (var item in idsList)
            {
                if (item !="")
                {
                    Marca_Categoria mc = new Marca_Categoria();
                    mc.Id_estado = 1; //queda activada
                    mc.Id_marca = int.Parse(item);
                    mc.Id_categoria = categoria.Id_categoria;
                    db.Marca_Categoria.Add(mc);
                    db.SaveChanges();
                }
            }
            if (ids == "")
            {
                return Json("noCategoria");
            }
            else
            {
                return Json("siCategoria");
            }
        }

        public IEnumerable<Marca> GetMarca()
        {
            
            var lista = db.Marca.ToList();
            return lista;
        }

        [HttpPost]
        public ActionResult ExisteCodigoNombreCategoria(string Codigo_categoria, string Nombre_categoria)
        {
            var categoria = db.Categoria.Where(d => d.Nombre_categoria.ToLower() == Nombre_categoria.ToLower()).ToList();
            if (categoria.Count > 0)
            {
                return Json("nombreExiste");
            }
            var categoria2 = db.Categoria.Where(d => d.Codigo_categoria == Codigo_categoria).ToList();
            if (categoria2.Count > 0)
            {
                return Json("codigoExiste");
            }
            return Json("");

        }

        //--------EDITAR

        public ActionResult EditarCategoria(int? id)
        {
            var lista = db.Categoria.Where(d => d.Id_categoria == id).ToList();
            if (id == null || lista.Count==0)
            {
                return Redirect(Url.Content("~/Categorias/ListarCategoria/"));
            }
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            Categoria categoria = db.Categoria.Find(id);
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado", categoria.Id_estado);
            ViewBag.Marcas = GetMarca();
            ViewBag.Marca_Categoria = GetMarcaCategoria();

            return View(categoria);
            
        }

        public IEnumerable<Marca_Categoria> GetMarcaCategoria()
        {
            var lista = db.Marca_Categoria.ToList();
            return lista;
        }

        [HttpPost]
        public ActionResult ExisteCodigoNombreCategoriaEdit(string Codigo_categoria, string Nombre_categoria,int Id_categoria)
        {
            var categoria = db.Categoria.Find(Id_categoria);
            var listaCategoriaA = db.Categoria.Where(d => d.Nombre_categoria == Nombre_categoria).ToList();
            var listaCategoriaB = db.Categoria.Where(d => d.Codigo_categoria == Codigo_categoria).ToList();

            if (listaCategoriaA.Count > 0 && categoria.Nombre_categoria.ToLower() != Nombre_categoria.ToLower())
            {
                return Json("nombreExiste");
            }
            if (listaCategoriaB.Count > 0 && categoria.Codigo_categoria.ToLower() != Codigo_categoria.ToLower())
            {
                return Json("codigoExiste");
            }
            return Json("");
            
        }

        //PENDIENTE VERIFICAR QUE ESTO FUNCINE BIEN Y SIN PROBLEMAS--->TRATAR DE HACER QUE SE CAIGA

        [HttpPost]
        public ActionResult EditarCategoriaAsociarCategoriaMarca(int Id_categoria,string ids,string idsDesactivar, string Codigo_categoria, string Nombre_categoria, int Id_estado, string Descripcion_categoria)
        {

            Categoria categoria = db.Categoria.Find(Id_categoria);
            categoria.Codigo_categoria = Codigo_categoria;
            categoria.Id_estado = Id_estado;
            categoria.Nombre_categoria = Nombre_categoria;
            categoria.Descripcion_categoria = Descripcion_categoria;
            db.Entry(categoria).State = EntityState.Modified;
            db.SaveChanges();

            //->Cambiar Estado asoc marca categoria
            string[] idsDesactivarArr = idsDesactivar.Split('%');
            List<string> idsDesactLst = idsDesactivarArr.ToList();
            

            foreach (var item in idsDesactLst)
            {
                if (item != "")
                {
                    int id_marca = int.Parse(item);
                    var mc = db.Marca_Categoria.Where(d => d.Id_categoria == Id_categoria && d.Id_marca == id_marca).FirstOrDefault();
                    if (mc != null)
                    {
                        mc.Id_estado = 2;
                        db.Entry(mc).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }
            }
            //->FIN CAMBIAR ESTADO ASOC MARCA CATEGORIA

            //NUEVA ASOC MARCA o reactivar
            string[] idsArr = ids.Split('%');
            List<string> idsList = idsArr.ToList();
            
            foreach (var item in idsList)
            {
                if (item != "")
                {
                    int id_marca = int.Parse(item);
                    var mc = db.Marca_Categoria.Where(d => d.Id_categoria == Id_categoria && d.Id_marca == id_marca).FirstOrDefault();
                    if (mc == null)
                    {
                        mc = new Marca_Categoria();
                        mc.Id_estado = 1; //queda activada
                        mc.Id_marca = id_marca;
                        mc.Id_categoria = categoria.Id_categoria;
                        db.Marca_Categoria.Add(mc);
                        db.SaveChanges();

                        
                    }
                    else
                    {
                        mc.Id_estado = 1;
                        db.Entry(mc).State = EntityState.Modified;
                        db.SaveChanges();

                        
                    }

                }
            }

            return Json("");
        }

        public ActionResult EstadoCategoria(int id)
        {
            var categoria = db.Categoria.Find(id);
            if (categoria.Id_estado == 1)
            {
                categoria.Id_estado = 2;
                db.Entry(categoria).State = EntityState.Modified;
            }
            else
            {
                categoria.Id_estado = 1;
                db.Entry(categoria).State = EntityState.Modified;
            }
            db.SaveChanges();
            return Redirect(Url.Content("~/Categorias/ListarCategoria"));
        }
    }
}