﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;
using System.Data.Entity;

namespace FunXgame.Controllers
{
    
    public class MarcasController : Controller
    {
        db_FunXGameEntities db = new db_FunXGameEntities();
        // GET: Marcas
        public ActionResult ListarMarca()
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            return View();
        }

        public ActionResult ListarMarcaPartial(string codigo, string nombre, int? estado)
        {
            if (codigo == "")
            {
                codigo = null;
            }
            if (nombre == "")
            {
                nombre = null;
            }
            var lista = db.Marca.ToList();

            if (codigo != null)
            {
                lista = lista.Where(d => d.Codigo_marca.ToLower() == codigo.ToLower()).ToList();
            }
            if (nombre != null)
            {
                lista = lista.Where(d => d.Nombre_marca.ToLower() == nombre.ToLower()).ToList();
            }
            if (estado != null)
            {
                lista = lista.Where(d => d.Id_estado == estado).ToList();
            }

            return PartialView("_ListarMarcaPartial", lista);

        }

        //---------------------Agregar---------------------------

        public ActionResult CrearMarca()
        {
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 2), "Id_imagen", "Nombre_imagen");
            return View();
        }
        [HttpPost]
        public ActionResult CrearMarca(Marca marca)
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }

            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 2), "Id_imagen", "Nombre_imagen");

            db.Marca.Add(marca);
            db.SaveChanges();
            return View();
        }

        //--------------------- valida si existe ----------------------
        [HttpPost]
        public ActionResult ExisteCodigoNombreMarca(string Codigo_Marca, string Nombre_marca)
        {
            var marca = db.Marca.Where(d => d.Nombre_marca.ToLower() == Nombre_marca.ToLower()).ToList();
            if (marca.Count > 0)
            {
                return Json("nombreExiste");
            }
            var marca2 = db.Marca.Where(d => d.Codigo_marca.ToLower() == Codigo_Marca.ToLower()).ToList();
            if (marca2.Count > 0)
            {
                return Json("codigoExiste");
            }
            return Json("");
        }

        public ActionResult CargaImgPreview(int? id)
        {
            if (id != null)
            {
                Imagen imagen = db.Imagen.Find(id);
                string ruta = imagen.Ruta;
                ruta = ruta.Substring(1);
                return Json(ruta);
            }
            return Json("");
        }

        //-----------------------------------------Editar------------------------------------------------------
        public ActionResult EditarMarca(int? id)
        {
            var lista = db.Marca.Where(d => d.Id_marca == id).ToList();
            if (id == null || lista.Count==0)
            {
                return Redirect(Url.Content("~/Marcas/ListarMarca/"));
            }

            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }

            Marca marca = db.Marca.Find(id);
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado", marca.Id_estado);
            ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 2), "Id_imagen", "Nombre_imagen", marca.Id_imagen);
            return View(marca);
        }

        [HttpPost]
        public ActionResult EditarMarca(Marca marca)
        {
            //ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado", marca.Id_estado);
            //ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 2), "Id_imagen", "Nombre_imagen", marca.Id_imagen);
            db.Entry(marca).State = EntityState.Modified;
            db.SaveChanges();
            return Redirect(Url.Content("~/Marcas/ListarMarca/"));
        }

        
        [HttpPost]
        public ActionResult CodigoNombreExisteEdit(string Codigo_marca, string Nombre_marca, int Id_Marca)
        {
            var marca = db.Marca.Find(Id_Marca);
            var listaMarcaA = db.Marca.Where(d => d.Nombre_marca == Nombre_marca).ToList();
            var listaMarcaB = db.Marca.Where(d => d.Codigo_marca == Codigo_marca).ToList();
            if (listaMarcaA.Count > 0 && marca.Nombre_marca.ToLower()!=Nombre_marca.ToLower())
            {
               return Json("nombreExiste");
            }
            if (listaMarcaB.Count > 0 && marca.Codigo_marca.ToLower()!=Codigo_marca.ToLower())
            {
                return Json("codigoExiste");
            }
            return Json("");
        }


        public ActionResult EstadoMarca(int id)
        {
            var marca = db.Marca.Find(id);
            if (marca.Id_estado == 1)
            {
                marca.Id_estado = 2;
                db.Entry(marca).State = EntityState.Modified;
            }
            else
            {
                marca.Id_estado = 1;
                db.Entry(marca).State = EntityState.Modified;
            }
            db.SaveChanges();
            return Redirect(Url.Content("~/Marcas/ListarMarca"));
        }

    }
}