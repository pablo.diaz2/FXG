﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;


namespace FunXgame.Controllers
{
    public class LogInController : Controller
    {
        
        // GET: Login
        
        public ActionResult LogIn()
        {
            return View();
        }

       [HttpPost]
       public ActionResult LogIn(Usuario model)
        {
            using (db_FunXGameEntities db = new db_FunXGameEntities())
            {
                var usrList = db.Usuario.Where(d => d.Email == model.Email && d.Contrasena == model.Contrasena);
                if (usrList.Count() > 0){

                    Usuario usuario = usrList.First();
                    if (usuario.Id_rol == 1){

                        Session["Admin"] = usuario.Primer_nombre;
                        Session["Id_admin"] = usuario.Id_usuario;
                        return Content(usuario.Id_rol.ToString());
                    }

                    if(usuario.Id_rol == 2)
                    {
                        Session["Cliente"] = usuario.Primer_nombre;
                        Session["Id_cliente"] = usuario.Id_usuario;
                        return Content(usuario.Id_rol.ToString());
                    }
                }
            }
            return Content("Verifique nombre de usuario y contraseña");
        }
    }
}