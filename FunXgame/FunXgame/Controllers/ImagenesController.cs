﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;

namespace FunXgame.Controllers
{
    
    public class ImagenesController : Controller
    {
        db_FunXGameEntities db = new db_FunXGameEntities();
        // GET: Imagenes
        //IMAGENES
        public ActionResult ListarImagen()
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            ViewBag.grupoImagen = new SelectList(db.Grupo_Imagen, "Id_grupo", "Nombre_grupo");

            return View();
        }
        
        public ActionResult ListarImagenPartial(int? id, string nombre, int? grupo)
        {
            if (nombre == "")
            {
                nombre = null;
            }

            var lista = db.Imagen.ToList();

            if (id != null)
            {
                lista = lista.Where(d => d.Id_imagen == id).ToList();
            }

            if (nombre!=null)
            {
                lista = lista.Where(d => d.Nombre_imagen.ToLower() == nombre.ToLower()).ToList();
            }

            if (grupo != null)
            {
                lista = lista.Where(d => d.Id_grupo == grupo).ToList();
            }
            return PartialView("_ListarImagenPartial", lista);
        }

        //----------------------------------------->CREACION<------------------------------------//
        public ActionResult CrearImagen()
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            ViewBag.Id_grupo = new SelectList(db.Grupo_Imagen, "Id_grupo", "Nombre_grupo");
            return View();
        }

        [HttpPost]
        public ActionResult CrearImagen(Imagen img, HttpPostedFileBase Ruta)
        {
            ViewBag.Id_grupo = new SelectList(db.Grupo_Imagen, "Id_grupo", "Nombre_grupo");
            try
            {
                string fileName = "";

                if (img.Ruta != null)
                {
                    string extension = Path.GetExtension(Ruta.FileName);
                    fileName = img.Nombre_imagen + extension;
                    img.Ruta = "~/Content/Assets/Imagenes/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Content/Assets/Imagenes/"), fileName);
                    Ruta.SaveAs(fileName);

                    db.Imagen.Add(img);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return Redirect(Url.Content("~/Imagenes/CrearImagen/"));
            }

            return View();
        }
        [HttpPost]
        public ActionResult ExisteImagen(string Nombre_imagen)
        {

            if (!string.IsNullOrEmpty(Nombre_imagen))
            {
                var p = db.Imagen.FirstOrDefault(d => d.Nombre_imagen.ToLower().Equals(Nombre_imagen.ToLower()));
                if (p != null)
                {
                    return Json(p.Nombre_imagen);
                }
            }
            return Json("");
        }

        ////////////////////////////////////////////////-> EDITAR<-//////////////////////////////////////////////////////////

        public ActionResult EditarImagen(int? id)
        {
            var lista = db.Imagen.Where(d => d.Id_imagen == id).ToList();
            if (lista.Count == 0)
            {
                return Redirect(Url.Content("~/Imagenes/ListarImagen/"));
            }
            if (id != null)
            {
                Imagen img = db.Imagen.Find(id);
                ViewBag.Id_grupo = new SelectList(db.Grupo_Imagen, "Id_grupo", "Nombre_grupo", img.Id_grupo);
                return View(img);
            }

            return Redirect(Url.Content("~/Imagenes/ListarImagen/"));
        }

        [HttpPost]
        public ActionResult EditarImagen(Imagen img, HttpPostedFileBase Ruta, string Id_Imagen, string Nombre_imagen)
        {
            
            if (img.Ruta == null)
            {
                string rutaActual = "";
                string nuevaRuta = "";
                using (db_FunXGameEntities db2 = new db_FunXGameEntities())
                {
                    int id = int.Parse(Id_Imagen);
                    Imagen imagen = db2.Imagen.Find(id);
                    rutaActual = imagen.Ruta;
                    if (System.IO.File.Exists(Server.MapPath(rutaActual)))
                    {
                        //A ESTO LE FALTA UN TRY CATCH....-> segun yo.....
                        string extension = Path.GetExtension(rutaActual);
                        nuevaRuta = "~/Content/Assets/Imagenes/" + img.Nombre_imagen + extension;
                        System.IO.File.Move(Server.MapPath(rutaActual), Server.MapPath(nuevaRuta));
                    }
                }
                img.Ruta = nuevaRuta;
                db.Entry(img).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(Url.Content("~/Imagenes/ListarImagen/"));
            }
            
            if (img.Ruta != null)
            {
                string rutaActual = "";
                //string nuevaRuta = "";

                using (db_FunXGameEntities db2 = new db_FunXGameEntities())
                {
                    int id = int.Parse(Id_Imagen);
                    Imagen imagen = db2.Imagen.Find(id);
                    rutaActual = imagen.Ruta;
                    if (System.IO.File.Exists(Server.MapPath(rutaActual)))
                    {
                        string extension1 = Path.GetExtension(rutaActual);
                        System.IO.File.Delete(Server.MapPath(rutaActual));
                        string fileName = "";
                        string extension2 = Path.GetExtension(Ruta.FileName);
                        string nombreImg = Nombre_imagen;
                        fileName = img.Nombre_imagen + extension2;
                        img.Ruta = "~/Content/Assets/Imagenes/" + fileName;
                        fileName = Path.Combine(Server.MapPath("~/Content/Assets/Imagenes/"), fileName);
                        Ruta.SaveAs(fileName);
                        img.Nombre_imagen = nombreImg;                       
                    }
                }
                db.Entry(img).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Redirect(Url.Content("~/Imagenes/ListarImagen/"));

        }

        [HttpPost]
        public ActionResult ExisteImagenEditar(string Nombre_imagen, string Id_imagen)
        {
            int id = int.Parse(Id_imagen);
            var img = db.Imagen.Where(d => d.Id_imagen == id && d.Nombre_imagen.Equals(Nombre_imagen)).FirstOrDefault();
            if (img != null)
            {
                if (img.Id_imagen == id && img.Nombre_imagen == Nombre_imagen) //->si es la misma imagen no te preocupes por el nombre
                {
                    return Json("");
                }
            }
            var p = db.Imagen.FirstOrDefault(d => d.Nombre_imagen.ToLower().Equals(Nombre_imagen.ToLower()));
            if (p != null)
            {
                return Json(p.Nombre_imagen);
            }
            else
            {
                return Json("");
            }
        }

        ////////////////////////////////////////////////-> ELIMINAR<-//////////////////////////////////////////////////////////
        [HttpPost]
        public ActionResult EliminarImagen(int id)
        {   
            Imagen imagen = db.Imagen.Find(id);
            //->validaciones cuando tenga marcas y productos asociados a las imagenes.....para que no se puedan borrar.
            try
            {
                string rutaActual = imagen.Ruta; //->ruta relativa = /~/ruta/otraruta
                if (System.IO.File.Exists(Server.MapPath(rutaActual)))
                {
                    db.Imagen.Remove(imagen);
                    db.SaveChanges();
                    System.IO.File.Delete(Server.MapPath(rutaActual));
                    return Json("");
                }
            }
            catch (Exception)
            {
                return Json("1");
            }
            return Json("1");
        }
    }

}