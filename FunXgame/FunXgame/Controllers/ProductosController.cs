﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;
using System.Data.Entity;

namespace FunXgame.Controllers
{
    public class ProductosController : Controller
    {
        db_FunXGameEntities db = new db_FunXGameEntities();
        // GET: Productos
        public ActionResult ListarProducto()
        {
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            ViewBag.Id_categoria = new SelectList(db.Categoria, "Id_categoria", "Nombre_categoria");
            ViewBag.Id_marca = new SelectList(db.Marca, "Id_marca", "Nombre_marca");
            
            return View();
        }
       

        public ActionResult ListarProductoPartial(string codigo, string nombre, int? categoria , int? marca, int? estado)
        {
            if (codigo == "")
            {
                codigo = null;
            }
            if (nombre == "")
            {
                nombre = null;
            }
            var lista = db.Producto.ToList();

            if (nombre != null)
            {
                lista = lista.Where(d => d.Nombre_producto.ToLower() == nombre.ToLower()).ToList();
            }

            if (codigo != null)
            {
                lista = lista.Where(d => d.Codigo_producto.ToLower() == codigo.ToLower()).ToList();
            }

            if (categoria != null)
            {
                lista = lista.Where(d => d.Id_categoria == categoria).ToList();
            }

            if (marca != null)
            {
                lista = lista.Where(d => d.Id_marca == marca).ToList();
            }

            if (estado != null)
            {
                lista = lista.Where(d => d.Id_estado == estado).ToList();
            }           

            return PartialView("_ListarProductoPartial",lista);
        }

        

        //---->Crear
        public ActionResult CrearProducto()
        {
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            ViewBag.Id_marca = new SelectList(db.Marca, "Id_marca", "Nombre_marca");
            //ViewBag.Id_categoria = new SelectList(db.Categoria, "Id_categoria", "Nombre_categoria");
            ViewBag.Marca_Categoria = GetMarcaCategoria();
            ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 1), "Id_imagen", "Nombre_imagen");
            return View();
        }

        public IEnumerable<Marca_Categoria> GetMarcaCategoria()
        {
            var lista = db.Marca_Categoria.ToList();
            return lista;
        }

        public ActionResult GetCategorias(int? id)
        {
            if (id != null)
            {
                db_FunXGameEntities db = new db_FunXGameEntities();
                return Json(db.Marca_Categoria.Where(d => d.Id_marca == id && d.Id_estado==1).Select(d => new
                {
                    nombre = d.Categoria.Nombre_categoria,
                    categoria = d.Id_categoria
                }).ToList(), JsonRequestBehavior.AllowGet);

            }
            return Json("");
        }


        [HttpPost]
        public ActionResult CrearProducto(Producto producto)
        {
            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado");
            ViewBag.Id_marca = new SelectList(db.Marca, "Id_marca", "Nombre_marca");
            
            ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 1), "Id_imagen", "Nombre_imagen");
            db.Producto.Add(producto);
            db.SaveChanges();
            return View();
        }

        [HttpPost]
        public ActionResult ExisteCodigoNombreProducto(string Codigo_producto, string Nombre_producto)
        {
            var producto = db.Producto.Where(d => d.Nombre_producto.ToLower() == Nombre_producto.ToLower()).ToList();
            if (producto.Count > 0)
            {
                return Json("nombreExiste");
            }
            var producto2 = db.Producto.Where(d => d.Codigo_producto.ToLower() == Codigo_producto.ToLower()).ToList();
            if (producto2.Count > 0)
            {
                return Json("codigoExiste");
            }
            return Json("");
        }

    //---->Editar
        
        public ActionResult EditarProducto(int? id)
        {
            var lista = db.Producto.Where(d => d.Id_producto == id).ToList();
            if (id == null || lista.Count==0)
            {
                return Redirect(Url.Content("~/Productos/ListarProducto"));
            }
            if (Session["Admin"] == null)
            {
                return Redirect(Url.Content("~/Home/Index/"));
            }
            var producto = db.Producto.Find(id);

            ViewBag.Id_estado = new SelectList(db.Estado.Where(d => d.Id_estado == 1 || d.Id_estado == 2), "Id_estado", "Nombre_estado",producto.Id_estado);
            ViewBag.Id_categoria = new SelectList(db.Marca_Categoria.Where(d=>d.Id_marca==producto.Id_marca && d.Id_estado==1), "Id_categoria", "Categoria.Nombre_categoria",producto.Id_categoria);
            ViewBag.Id_marca = new SelectList(db.Marca, "Id_marca", "Nombre_marca",producto.Id_marca);
            ViewBag.Id_imagen = new SelectList(db.Imagen.Where(d => d.Id_grupo == 1), "Id_imagen", "Nombre_imagen",producto.Id_imagen);

            return View(producto);
        }

        

        [HttpPost]
        public ActionResult EditarProducto(Producto producto)
        {
            
            db.Entry(producto).State = EntityState.Modified;
            db.SaveChanges();
            return Redirect(Url.Content("~/Productos/ListarProducto/"));
        }


        [HttpPost]
        public ActionResult CodigoNombreExisteEdit(string Codigo_producto, string Nombre_producto, int Id_producto)
        {
            var producto = db.Producto.Find(Id_producto);
            var listaProductoA = db.Producto.Where(d => d.Nombre_producto == Nombre_producto).ToList();
            var listaProductoB = db.Producto.Where(d => d.Codigo_producto == Codigo_producto).ToList();
            if (listaProductoA.Count > 0 && producto.Nombre_producto.ToLower() != Nombre_producto.ToLower())
            {
                return Json("nombreExiste");
            }
            if (listaProductoB.Count > 0 && producto.Codigo_producto.ToLower() != Codigo_producto.ToLower())
            {
                return Json("codigoExiste");
            }
            return Json("");
        }



        public ActionResult CargaImgPreview(int? id)
        {
            if (id != null)
            {
                Imagen imagen = db.Imagen.Find(id);
                string ruta = imagen.Ruta;
                ruta = ruta.Substring(1);
                return Json(ruta);
            }
            return Json("");
        }

        public ActionResult CapturaEstadoMarca(int? id)
        {
            if (id != null)
            {
                var marca = db.Marca.Find(id);
                if (marca.Id_estado == 2)
                {
                    return Json("marcaDesactivada");
                }
            }
            return Json("");
        }

        public ActionResult CapturaEstadoCategoria(int? idMarca, int? idCategoria)
        {
            
            if (idCategoria != null)
            {
                var categoria = db.Categoria.Find(idCategoria);
                if (categoria.Id_estado == 2)
                {
                    return Json("categoriaDesactivada");
                }
            }
            
            return Json("");
        }

        public ActionResult EstadoProducto(int id)
        {
            var producto = db.Producto.Find(id);
            if (producto.Id_estado == 1)
            {
                producto.Id_estado = 2;
                db.Entry(producto).State = EntityState.Modified;
            }
            else
            {
                producto.Id_estado = 1;
                db.Entry(producto).State = EntityState.Modified;
            }
            db.SaveChanges();
            return Redirect(Url.Content("~/Productos/ListarProducto"));
        }
    }
}