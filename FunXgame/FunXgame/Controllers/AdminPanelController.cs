﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunXgame.Models;

namespace FunXgame.Controllers
{
    
    public class AdminPanelController : Controller
    {
        // GET: AdminPerfil
        db_FunXGameEntities db = new db_FunXGameEntities();

        public ActionResult AdminPanelMenu()
        {
            return PartialView("_AdminPanelMenu");
        }

        public ActionResult AdminPerfil()
        {
            if (Session["Admin"] != null)
            {
                int id = int.Parse(Session["Id_Admin"].ToString());
                Usuario usr = db.Usuario.Find(id);
                return View(usr);
            }
            return Redirect(Url.Content("~/Home/Index/"));
        }

        [HttpPost]
        public ActionResult AdminPerfil(Usuario usr)
        {

            usr.Id_rol = 1;
            db.Entry(usr).State = EntityState.Modified;
            db.SaveChanges();

            Session.Abandon(); //---->abandona sesión para que vuelva a ingresar con 

            return Redirect(Url.Content("~/LogIn/logIn"));
        }



       





    }
}